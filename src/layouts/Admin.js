import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components

import AdminNavbar from "components/Admin/Include/AdminNavbar.js";
import Sidebar from "components/Admin/Include/Sidebar.js";
import FooterAdmin from "components/Admin/Include/FooterAdmin.js";

// views

import Dashboard from "components/Admin/Dashboard/Dashboard.js";

// Bank
import Bank from "components/Admin/Bank/IndexBank.js";
import BankForm from "components/Admin/Bank/FormBank.js";
import BankEdit from "components/Admin/Bank/EditBank.js";

// // Topup
import Topup from "components/Admin/Topup/IndexTopup.js";
import TopupForm from "components/Admin/Topup/FormTopup.js";
// import TopupEdit from "components/Admin/Topup/EditTopup.js";


export default function Admin() {
  

  return (
    <>
      <Sidebar />
      <div className="relative md:ml-64 bg-blueGray-100">
        <AdminNavbar />
        <div className="px-4 md:px-10 mx-auto w-full -m-24">
          <Switch>
            {/* <Route path="/admin/dashboard" exact component={Dashboard} /> */}

            <Route path="/admin/bank" exact component={Bank} />
            <Route path="/admin/bank/form" exact component={BankForm} />
            <Route path="/admin/bank/edit/:id" component={BankEdit} />

            <Route path="/admin/Topup/histry" exact component={Topup} />
            <Route path="/admin/Topup" exact component={TopupForm} />
            {/* <Route path="/admin/Topup/edit/:id" component={TopupEdit} /> */}

            <Redirect from="/" to="/admin/bank" />
          </Switch>
          <FooterAdmin />
        </div>
      </div>
    </>
  );
}
