import React , {  useEffect , useState } from "react";
import { Link   } from "react-router-dom";
import Swal from "sweetalert2";

// services
import TopupServices from "../../../services/Admin/Topup"
import bankServices from "../../../services/Admin/Bank"


export default function FormEmployee(props) {

  const [ channel, setChannel ] = useState(null);
  const [ amount, setAmount ] = useState(null);
  const [ bank_id, setBank ] = useState(null);
  const [ listBank, setListBank ] = useState([]);

  useEffect(()=>{

    async function fetchDataBank(){
      const res = await bankServices.listBank();
      setListBank(res.data);
    }

    fetchDataBank();
  },[]);

  const saveTopup = async () => {

    const data = {
      channel,
      amount,
      bank_id
    }
    const res = await TopupServices.save(data);
    if (res.success) {
      
      Swal.fire({
          title: "บันทึกข้อมูลสำเร็จ",
          icon: "success",
          text: "",
          confirmButtonText: "ตกลง"
      }).then(function(result) {

        if (result.value) {
          props.history.push('/admin/topup/histry');
        }

      });
    }
    else {
      
      Swal.fire({
          text: res.message,
          icon: "error",
          confirmButtonText: "ตกลง"
      });		
    }
    
  }
  return (
    <>
    <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">เติมเงินเข้าบัญชีกระเป๋าเงินอิเล็กทรอนิกส์</h6>
            <Link to="/admin/topup/histry">
            <button
              className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="button"
            >
              ประวัติการเติมเงิน
            </button>
            </Link>
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
       
            
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    จํานวนเงิน : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    onChange={(event)=>setAmount(event.target.value)} 
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    ช่องทาง : 
                  </label>
                  <select id="inputState2" defaultValue="" className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" 
                    onChange={(event)=> setChannel(event.target.value)}>
                    <option  selected>== เลือกรายการ ==</option>
                    <option  value='TrueMoney'>TrueMoney</option>
                    <option  value='โอนเงิน'>โอนเงิน</option>
                    <option  value='บัตรเครดิต'>บัตรเครดิต</option>
                    
                  </select>
                </div>
              </div>
            </div>
            
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    บัญชีกระเป๋าเงินอิเล็กทรีอนิกส์ : 
                  </label>
                  <select id="inputState" defaultValue="" className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" 
                    onChange={(event)=> setBank(event.target.value)}>
                    <option  selected>== เลือกรายการ ==</option>
                    {
                      listBank.map((item,i)=>{
                          return(
                            <option key={i} value={item.id}>{item.bank_name}</option>
                          )
                      })
                    }
                  </select>
                </div>
              </div>
              
            </div>
            {/* <hr className="mt-2 mb-2 border-b-1 border-blueGray-300" /> */}
            <div className="mt-2 flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                <button className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="submit"
          onClick={()=>saveTopup()}>บันทึกข้อมูล</button>
                </div>
              </div>
            </div>
        </div>
      </div>
        </div>
        
      </div>
      
    </>
  );
}
