import React , { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import Moment from 'react-moment';
import 'moment-timezone';

// services
import topupServices from "../../../services/Admin/Topup"


export default function IndexTopup(props) {
  const [ listTopup, setListTopup ] = useState([]);
  useEffect(()=>{

    async function fetchDataTopup(){
      const res = await topupServices.list();
      console.log(res)
      setListTopup(res.data);
    }

    fetchDataTopup();
  },[]);
  
 

  
 
  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
            <div className="rounded-t bg-white mb-0 px-6 py-6">
              <div className="text-center flex justify-between">
                <h6 className="text-blueGray-700 text-xl font-bold">เติมเงินเข้าบัญชีกระเป๋าเงินอิเล็กทรอนิกส์</h6>
                <Link to="/admin/topup">
                <button
                  className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
                  type="button"
                >
                  เพิ่มรายการ
                </button>
                </Link>
              </div>
            </div>
            <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
          
              <table className="items-center w-full bg-white border-collapse table-auto">
                <thead>
                  <tr >
                  <th
                      className="
                        px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100
                      "
                    >
                      ลำดับ
                    </th>
                    <th
                      className=
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      
                    >
                      รหัสอ้างอิง
                    </th>
                    <th
                      className=
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      
                    >
                      ช่องทาง
                    </th>
                    <th
                      className=
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      
                    >
                      บัญชีกระเป๋าเงินอิเล็กทรีอนิกส์
                    </th>
                    <th
                      className=
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      
                    >
                      จํานวนเงิน (บาท)
                    </th>
                    <th
                      className=
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      
                    >
                      วันเวลาทำรายการ
                    </th>
                    
                  </tr>
                </thead>
                <tbody>
                {
                  listTopup.map((item,i)=>{
                    return(
                        <tr key={i}>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-center" width='5'>
                          {i+1}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center" width='5'>
                            
                            <span
                              className=
                                " font-bold text-blueGray-600"
                              
                            >
                              {item.resf_code}
                            </span>
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='10'>
                          {item.channel}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='10'>
                          {item.bank ? item.bank.bank_name : ''}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='5'>
                          {item.amount}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='5'>
                          <Moment format="DD/MM/YYYY , hh:mm">{item.updated_at}</Moment>
                          </td>
                          
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
