import React , { useEffect, useState } from "react";
import { Link  } from "react-router-dom";
import Swal from "sweetalert2";

// services
import employeesServices from "../../../services/Admin/Topup"
import roleServices from "../../../services/Admin/Bank"


export default function EditEmployee(props) {
  const [ id, setId ] = useState(null);
  const [ fname, setFname ] = useState(null);
  const [ lname, setLname ] = useState(null);
  const [ email, setEmail ] = useState(null);
  const [ phone, setPhone ] = useState(null);
  const [ rol, setRol ] = useState(null);
  const [ selectedRol , setSelectRol ] = useState(null);
  const [ listRole, setListRole ] = useState([]);

  useEffect(()=>{

    async function fetchDataEmployee(){
      let id = props.match.params.id;
      const res = await employeesServices.get(id);

      if (res.success) {
        //console.log(res);
        const data = res.data
        setId(data.id)
        setFname(data.fname)
        setLname(data.lname)
        setEmail(data.email)
        setPhone(data.phone)
        setRol(data.rol)
        setSelectRol((data.role ? data.role.rol_name : null ))
      }
      else {
        alert(res.message)
      }
    }
    fetchDataEmployee();

  },[]);

  useEffect(()=>{

    async function fetchDataRole(){
      const res = await roleServices.listRole();
      setListRole(res.data);
    }

    fetchDataRole();
  },[]);


  const updateEmployee = async () => {

    const data = {
      id,
      fname,
      lname,
      email,
      phone, 
      rol
    }

    const res = await employeesServices.update(data);
    
    if (res.success) {
      // alert(res.message)
      Swal.fire({
          title: "แก้ไขข้อมูลสำเร็จ",
          icon: "success",
          text: "",
          confirmButtonText: "ตกลง"
      }).then(function(result) {

        if (result.value) {
          props.history.push('/admin/employees');
        }

      });
      
    }
    else {
      Swal.fire({
          text: res.message,
          icon: "error",
          confirmButtonText: "ตกลง"
      });		
    }

  }
  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">เติมเงินเข้าบัญชีกระเป๋าเงินอิเล็กทรอนิกส์</h6>
            <Link to="/admin/employees">
            <button
              className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="button"
            >
              รายการ
            </button>
            </Link>
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
       
            
        <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    ชื่อ : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    value={fname}
                    onChange={(event)=>setFname(event.target.value)} 
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    นามสกุล : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    value={lname}
                    onChange={(event)=>setLname(event.target.value)} 
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    อีเมล์ : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    value={email}
                    onChange={(event)=>setEmail(event.target.value)} 
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    เบอร์โทร : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    value={phone}
                    onChange={(event)=>setPhone(event.target.value)} 
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    สิทธิการใช้งาน : 
                  </label>
                  <select id="inputState" className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150" value={rol}
                  onChange={(event)=>setRol(event.target.value)}>
                    <option >== เลือกรายการ ==</option>
                    {
                      listRole.map((itemselect)=>{
                        if(itemselect.active === 1){  
                          return(
                            <option value={itemselect.id}>{itemselect.rol_name}</option>
                          )
                        }
                      })
                    }
                  </select>
                </div>
              </div>
              
            </div>
            {/* <hr className="mt-2 mb-2 border-b-1 border-blueGray-300" /> */}
            <div className="mt-2 flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                <button className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="submit"
          onClick={()=>updateEmployee()}>แก้ไขข้อมูล</button>
                </div>
              </div>
            </div>
        </div>
      </div>
        </div>
        
      </div>
      
    </>
  );
}
