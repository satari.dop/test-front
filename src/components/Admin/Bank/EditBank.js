import React , { useEffect, useState } from "react";
import { Link  } from "react-router-dom";
import Swal from "sweetalert2";

// services
import bankServices from "../../../services/Admin/Bank"

export default function EitBank(props) {
  
  const [ id, setId ] = useState(null);
  const [ bank_number, setBankNumber ] = useState(null);
  const [ bank_name, setBankName] = useState(null);
  
 

  useEffect(()=>{

    async function fetchDataRole(){

      let id = props.match.params.id;
      const res = await bankServices.get(id);
      if (res.success) {
        //console.log(res);
        const data = res.data
        setId(data.id)
        setBankNumber(data.bank_number)
        setBankName(data.bank_name)
      }
      else {
        alert(res.message)
      }
    }
    fetchDataRole();

  },[]);


  const updateBank = async () => {
    
    const data = {
      id, bank_number , bank_name
    }

    const res = await bankServices.update(data);

    if (res.success) {
      // alert(res.message)
      Swal.fire({
          title: "แก้ไขข้อมูลสำเร็จ",
          icon: "success",
          text: "",
          confirmButtonText: "ตกลง"
      }).then(function(result) {

        if (result.value) {
          props.history.push('/admin/bank');
        }

      });
      
    }
    else {
      Swal.fire({
          text: res.message,
          icon: "error",
          confirmButtonText: "ตกลง"
      });		
    }

  }

  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">บัญชีกระเป๋าเงินอิเล็กทรอนิกส์</h6>
            <Link to="/admin/bank">
            <button
              className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="button"
            >
              รายการ
            </button>
            </Link>
          </div>
        </div>
        
        <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    เลขที่บัญชี : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    defaultValue={bank_number}
                    onChange={(event)=>setBankNumber(event.target.value)}
                  />
                </div>
              </div>
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    ชื่อบัญชี : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    
                    defaultValue={bank_name}
                    onChange={(event)=>setBankName(event.target.value)}
                  />
                </div>
              </div>
            </div>
            
            {/* <hr className="mt-2 mb-2 border-b-1 border-blueGray-300" /> */}
            <div className="mt-2 flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                <button className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="submit"
          onClick={()=>updateBank()}>แก้ไขข้อมูล</button>
                </div>
              </div>
            </div>
        </div>
      </div>
        </div>
        
      </div>
      
    </>
  );
}
