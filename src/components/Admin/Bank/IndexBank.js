import React , { useEffect, useState , useMemo } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import Moment from 'react-moment';
import 'moment-timezone';

// services
import bankServices from "../../../services/Admin/Bank"


export default function IndexBank() {
  
  const [ listBank, setListBank ] = useState([]);
  useEffect(()=>{

    async function fetchDataBank(){
      const res = await bankServices.list();
      setListBank(res.data);
    }

    fetchDataBank();
  },[]);
  
  const onClickDeleteConfirm = async (i,id) => {
      Swal.fire({
        title: "ยืนยันการการลบข้อมูล ?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
        onOpen: () => {
            // code
        }
      }).then((result) => {
          if (result.value) {
             onClickDelete(i,id)
          }else{
             setListBank(listBank)
          }
      });
      
  }

  const onClickDelete = async (i,id) => {
      const res = await bankServices.delete(id)
      if (res.success) {
        setListBank(listBank.filter(item => item.id !== id));
        Swal.fire({
          title: "ลบข้อมูลเรียบร้อย",
          icon: "success",
          confirmButtonText: 'ตกลง',
        })
      }
      else{
        alert(res.message);
      }
  }
 
  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
            <div className="rounded-t bg-white mb-0 px-6 py-6">
              <div className="text-center flex justify-between">
                <h6 className="text-blueGray-700 text-xl font-bold">บัญชีกระเป๋าเงินอิเล็กทรอนิกส์</h6>
                <Link to="/admin/bank/form">
                <button
                  className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
                  type="button"
                >
                  เพิ่มรายการ
                </button>
                </Link>
              </div>
            </div>
            <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
          
              <table className="items-center w-full bg-white border-collapse table-auto">
                <thead>
                  <tr>
                    <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" >
                      ลำดับ
                    </th>
                    <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" >
                    เลขที่บัญชี
                    </th>
                    <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" >
                    ชื่อบัญชี
                    </th>
                    <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" >
                      อัพเดทข้อมูล
                    </th>
                    <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100">

                    </th>
                  </tr>
                </thead>
                <tbody>
                {
                  listBank.map((item,i)=>{
                    return(
                        <tr key={i}>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-center" width='5'>
                          {i+1}
                          </td>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                            <span className= " font-bold text-blueGray-600" >
                              {item.bank_number}
                            </span>
                          </th>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='5'>
                            {item.bank_name}
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left" width='5'>
                          <Moment format="DD/MM/YYYY , hh:mm">{item.updated_at}</Moment>
                          </td>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                          <Link to={"/admin/bank/edit/"+item.id} className="bg-yellow-500 text-white active:bg-yellow-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"> <i className="fas fa-edit"></i> </Link>
                          <a href="#" className="bg-red-500 text-white active:bg-red-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150" onClick={()=>onClickDeleteConfirm(i,item.id)}> <i className="fas fa-trash"></i> </a>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
