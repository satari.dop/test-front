import axios from "axios";
const baseUrl = "http://localhost:8000/api/topup"
const topup = {};

topup.save = async (data) => {
  const urlSave= baseUrl
  const res = await axios.post(urlSave,data)
  .then(response=> {return response.data })
  .catch(error=>{ return error; })
  return res;
}

topup.list = async () => {
  const urlList = baseUrl
  const res = await axios.get(urlList)
  .then(response=>{ return response.data; })
  .catch(error=>{ return error; })
  return res;
}


topup.get = async (id) => {

  const urlGet = baseUrl+"/"+id
  const res = await axios.get(urlGet)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

topup.update = async (data) => {
  const urlUpdate = baseUrl+"/"+data.id
  const res = await axios.put(urlUpdate,data)
  .then(response=>{ return response.data; })
  .catch(error =>{ return error; })
  return res;
}

topup.delete = async (id) => {
  const urlDelete = baseUrl+"/"+id
  const res = await axios.delete(urlDelete)
  .then(response=> { return response.data })
  .catch(error =>{ return error })
  return res;
}
  
export default topup
