import axios from "axios";
const baseUrl = "http://localhost:8000/api/bank"
const bank = {};

bank.save = async (data) => {
  const urlSave= baseUrl
  const res = await axios.post(urlSave,data)
  .then(response=> {return response.data })
  .catch(error=>{ return error; })
  return res;
}

bank.list = async () => {
  const urlList = baseUrl
  const res = await axios.get(urlList)
  .then(response=>{ return response.data; })
  .catch(error=>{ return error; })
  return res;
}
bank.listBank = async () => {
  const urlList = baseUrl
  const res = await axios.get(urlList)
  .then(response=>{ return response.data; })
  .catch(error=>{ return error; })
  return res;
}

bank.get = async (id) => {

  const urlGet = baseUrl+"/"+id
  const res = await axios.get(urlGet)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

bank.update = async (data) => {
  //console.log(data)
  const urlUpdate = baseUrl+"/"+data.id
  const res = await axios.put(urlUpdate,data)
  .then(response=>{ return response.data; })
  .catch(error =>{ return error; })
  return res;
}

bank.delete = async (id) => {
  const urlDelete = baseUrl+"/"+id
  const res = await axios.delete(urlDelete)
  .then(response=> { return response.data })
  .catch(error =>{ return error })
  return res;
}
  
export default bank
